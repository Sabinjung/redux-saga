import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { postMyMessage } from './actions';

import {
  getMessage
} from './selectors';


const mapDispatchToProps = dispatch => ({
  postMessage: myMessage => dispatch(postMyMessage(myMessage)),
});

const mapStateToProps = createStructuredSelector({
  myMessage: getMessage()
});


const Show = (props) => {
  const changeMessage = () => {
    props.postMessage('Changed')
  }
  return (
    <React.Fragment>
      <h1>{props.myMessage}</h1>
      <button onClick={changeMessage}>CLick ME</button>
    </React.Fragment>
  );
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Show);
