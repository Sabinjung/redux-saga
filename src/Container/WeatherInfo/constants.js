export const POST_LOCATION_REQUEST =
  'App/Containers/WeatherInfo/POST_LOCATION_REQUEST';
export const POST_LOCATION_SUCCESS =
  'App/Containers/WeatherInfo/POST_LOCATION_SUCCESS';
export const POST_LOCATION_FAILURE =
  'App/Containers/WeatherInfo/POST_LOCATION_FAILURE';
  export const POST_LOCATION_MESSAGE =
  'App/Containers/WeatherInfo/POST_LOCATION_MESSAGE';
