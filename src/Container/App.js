import React from 'react';
import WeatherInfo from './WeatherInfo';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Weather App</h2>
      </header>
      <WeatherInfo />
    </div>
  );
}

export default App;
