import { combineReducers } from 'redux';
import {weatherInfoReducer, messageReducer} from '../Container/WeatherInfo/reducers';

export default combineReducers({
  weatherInfo: weatherInfoReducer,
  messageInfo: messageReducer
});
